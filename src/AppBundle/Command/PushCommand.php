<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class PushCommand
 * @package AppBundle\Command
 */
class PushCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setDescription('Push emails')
            ->setName('pusher:push');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('pusher.pusher')->push();
        $output->writeln('News was send');
    }
}