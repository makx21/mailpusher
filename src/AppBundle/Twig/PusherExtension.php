<?php

namespace AppBundle\Twig;

use AppBundle\Entity\News;
use AppBundle\Entity\User;
use Symfony\Component\Routing\Router;

/**
 * Class PusherExtension
 * @package AppBundle\Twig
 */
class PusherExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    protected $router;

    /**
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }
    /**
     * @return array
     */
    public function getFunctions()
    {
        return array(
            'parseStatus' => new \Twig_Function_Method($this, 'parseStatus'),
            'sendingStatus' =>  new \Twig_Function_Method($this, 'sendingStatus'),
            'createUpDown' =>  new \Twig_Function_Method($this, 'createUpDown'),
            'parseSub' =>  new \Twig_Function_Method($this, 'parseSub'),
        );
    }

    /**
     * @param User $user
     */
    public function parseSub(User $user)
    {
        $label = $user->getIsSubscribed() ? 'success' : 'default';
        $text =  $user->getIsSubscribed() ? 'Yes' : 'No';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     * @return string
     */
    public function parseStatus(News $news)
    {
        $label = $news->getParsed() ? 'success' : 'default';
        $text =  $news->getParsed() ? 'OK' : 'Waiting';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     * @return string
     */
    public function sendingStatus(News $news)
    {
        $label = $news->getSending() ? 'success' : 'default';
        $text =  $news->getSending() ? 'OK' : 'Waiting';

        $status = str_replace(array('%label%', '%text%'), array($label, $text), '<span class="label label-%label%">%text%</span>');

        echo $status;
    }

    /**
     * @param News $news
     */
    public function createUpDown(News $news)
    {
        $upPath = $this->router->generate("pusher_admin_up_news", array('id' => $news->getId()));
        $downPath = $this->router->generate("pusher_admin_down_news", array('id' => $news->getId()));

        $result = str_replace(
            array('%pathUp%', '%pathDown%'),
            array($upPath, $downPath),
            '<a href="%pathUp%" title="Up"><span class="glyphicon glyphicon-arrow-up"></span></a>'.
            '<a href="%pathDown%" title="Down"><span class="glyphicon glyphicon-arrow-down"></span></a>'
        );

        echo $result;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pusher_extension';
    }
}