<?php

namespace AppBundle\Twig;

use Twig_Environment;

/**
 * Class TwigMailGenerator
 * @package AppBundle\Twig
 */
class TwigMailGenerator
{
    protected $twig;

    /**
     * @param Twig_Environment $twig
     */
    public function __construct(Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * Return rendered Swift Message
     *
     * @param string $identifier
     * @param array  $parameters
     *
     * @return mixed
     */
    public function getMessage($identifier, $parameters = array())
    {
        $template = $this->twig->loadTemplate('views/'.$identifier.'.html.twig');

        $this->generateUnsubscribeLink($parameters);
        $bodyHtml = $template->renderBlock('body_html', $parameters);

        $setTo = array($parameters['user']->getEmail() => $parameters['user']->getUsername());

        return \Swift_Message::newInstance()
            ->setSubject($parameters['news']->getContent()->getTitle())
            ->setContentType("text/html")
            ->setFrom('no-reply@app.com')
            ->setTo($setTo)
            ->setBody($bodyHtml, 'text/html');
    }

    /**
     * Generate unsubscribe link
     *
     * @param array $parameters
     * @return string
     */
    public function generateUnsubscribeLink(array &$parameters)
    {
        $id = $parameters['user']->getId();
        $secret = $parameters['secret'];
        $host = $parameters['host'];

        $hash = md5($id.$secret);
        $parameters['unsubscribe'] = $host.'/unsubscribe?id='.$id.'&s='.$hash;
    }
}
