<?php

namespace AppBundle\Parsing;

use Doctrine\ORM\EntityManager;

use Swift_Mailer;
use Twig_Environment;
use AppBundle\Twig\TwigMailGenerator;

/**
 * Class Pusher
 * @package AppBundle\Parsing
 */
class Pusher
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var \Twig_Environment
     */
    protected $twig;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $host;

    /**
     * @param EntityManager    $em
     * @param Swift_Mailer     $mailer
     * @param Twig_Environment $twig
     * @param string           $secret
     * @param string           $host
     */
    public function __construct(EntityManager $em, Swift_Mailer $mailer, Twig_Environment $twig, $secret, $host)
    {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->twig = $twig;

        $this->secret = $secret;
        $this->host = $host;
    }

    /**
     * Pushing emails
     */
    public function push()
    {
        $userEmails = $this->em->getRepository('AppBundle:User')->getNewUsersEmailForPushing();

        if ($this->checkDate()) {
            $oldUsers = $this->em->getRepository('AppBundle:User')->getOldUsersEmailForPushing();
            $userEmails = array_merge($userEmails, $oldUsers);
        }

        $news = $this->em->getRepository('AppBundle:News')->getNewsForPushing();

        if (!empty($news) && !empty($userEmails)) {
            $news = array_shift($news);
            $generator = new TwigMailGenerator($this->twig);

            foreach ($userEmails as $user) {
                $message = $generator->getMessage('mail', array(
                    'user' => $user,
                    'news' => $news,
                    'secret' => $this->secret,
                    'host' => $this->host,
                ));

                $this->mailer->send($message);
                $user->setMailCount($user->getMailCount() + 1);
                $this->em->persist($user);
            }

            $news->setSending(1);
            $this->em->persist($news);

            $this->em->flush();
        }
    }

    /**
     * @return bool
     */
    public function checkDate()
    {
        return (1 === (int) date('N'));
    }
}