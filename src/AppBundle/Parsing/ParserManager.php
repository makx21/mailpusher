<?php

namespace AppBundle\Parsing;

use AppBundle\Entity\Content;
use AppBundle\Parsing\Graber;
use AppBundle\Parsing\Parser;
use AppBundle\Events\ParserErrorEvent;

use AppBundle\PusherEvents;
use Doctrine\ORM\EntityManager;
use Guzzle\Http\Message\Response;
use Symfony\Component\EventDispatcher\EventDispatcherInterface as Dispatcher;

/**
 * Class ParserManager
 * @package AppBundle\Parsing
 */
class ParserManager
{
    /**
     * @var Graber
     */
    protected $graber;

    /**
     * @var Parser
     */
    protected $parser;

    /**
     * @var Uploader
     */
    protected $uploader;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var Dispatcher
     */
    protected $eventDispatcher;

    /**
     * @param Graber        $graber
     * @param Parser        $parser
     * @param Uploader      $uploader
     * @param EntityManager $em
     * @param Dispatcher    $eventDispatcher
     */
    public function __construct(Graber $graber, Parser $parser, Uploader $uploader, EntityManager $em, Dispatcher $eventDispatcher)
    {
        $this->graber = $graber;
        $this->parser = $parser;
        $this->uploader = $uploader;
        $this->em = $em;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Run parser manager
     */
    public function run()
    {
        $notParsedNews = $this->em->getRepository('AppBundle:News')->getNewsForParsing();

        if (!empty($notParsedNews)) {
            foreach ($notParsedNews as $news) {
                $content = $this->graber->getData($news->getUrl());

                if (!$this->checkContent($content)) {
                    continue;
                }

                $results = $this->parser->parse($content->getBody(true));

                $this->checkParserResults($results);

                $content = $news->getContent() ?: new Content();

                $content->setTitle($results['title']);

                if (!empty($results['images'])) {
                    $imgPaths = $this->uploadImages(array(
                            'item' => $news,
                            'images' => $results['images'],
                    ));

                    $results['body'] = $this->replaceImgBody($results['body'], $imgPaths);
                }

                $content->setBody($results['body']);

                $this->em->persist($content);
                $news->setContent($content);
                $news->setParsed(1);

            }
            $this->em->flush();
        }

    }

    /**
     * @param mixed $content
     * @return bool
     */
    public function checkContent($content)
    {
        if (!$content instanceof Response) {
            $this->eventDispatcher->dispatch(PusherEvents::PARSER_ERROR, new ParserErrorEvent($content));

            return false;
        }

        return true;
    }

    /**
     * @param array $results
     */
    public function checkParserResults(array $results)
    {
        if ('' === $results['title']) {
            $this->eventDispatcher->dispatch(PusherEvents::PARSER_ERROR, new ParserErrorEvent('Could not make title parse'));
        }

        if ('' === $results['body']) {
            $this->eventDispatcher->dispatch(PusherEvents::PARSER_ERROR, new ParserErrorEvent('Not recognized body'));
        }
    }

    /**
     * @param array $options
     * @return array
     */
    public function uploadImages(array $options)
    {
        $this->uploader->setNews($options['item']);

        return $this->uploader->run($options['images']);
    }

    /**
     * @param string $body
     * @param array  $paths
     * @return mixed
     */
    public function replaceImgBody($body, array $paths)
    {
        preg_match_all('/<img[^>]+>/i', $body, $images);

        return str_replace($images[0], $this->createReplaceTemplate($paths), $body);
    }

    /**
     * @param $paths
     * @return array
     */
    private function createReplaceTemplate($paths)
    {
        $count = count($paths);

        $replacement = array();
        $template = '<img src="%src%" hspace="6" vspace="6" />';

        for ($i = 0; $i < $count; $i++) {
            $replacement[] = str_replace('%src%', $paths[$i], $template);
        }

        return $replacement;
    }
}