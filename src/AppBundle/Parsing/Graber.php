<?php

namespace AppBundle\Parsing;

use Guzzle\Service\Client;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\CurlException;

/**
 * Class Graber
 * @package AppBundle\Parsing
 */
class Graber
{
    protected $guzzle;

    /**
     * @param Client $guzzle
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    /**
     * @param string $url
     * @return \Guzzle\Http\EntityBodyInterface|string
     */
    public function getData($url)
    {
        $request = $this->guzzle->get($url);
        try {
            $response = $request->send();
        } catch (ClientErrorResponseException $e) {
            return 'Page not found';
        }
        catch (CurlException $e) {
            return 'Curl error';
        }

        return $response;
    }
}
