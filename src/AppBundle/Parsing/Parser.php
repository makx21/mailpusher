<?php

namespace AppBundle\Parsing;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Parser
 * @package AppBundle\Parsing
 */
class Parser
{
    /**
     * @var array $allowableTags
     */
    protected $allowableTags;

    /**
     * @param array $allowableTags
     */
    public function __construct(array $allowableTags)
    {
        $this->allowableTags = $allowableTags;
    }

    /**
     * Do news parsing
     *
     * @param string $content
     * @return array
     */
    public function parse($content)
    {
        $parser = new Crawler($content);
        $results = array();

        // Find Title
        try {
            $title = $parser->filter('div#page_header h1')->children()->text();
        } catch (\Exception $e) {
            $title = '';
        }
        $results['title'] = $title;

        try {
            //Find body
            $body = $parser->filter('#article_body_container #article_body')->html();
            // Remove unused tags
            $body = strip_tags($body, implode('', $this->allowableTags));
            // Cut <a> tags but reserve anchor text
            $body = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $body);
            // Extract images
            $images = $this->extractImages($body);
        } catch (\Exception $e) {
            $body = '';
        }

        $results['body'] = trim($body);
        $results['images'] = isset($images[0]) ? $images[0] : array();

        return $results;
    }

    /**
     * @param $body
     * @return mixed
     */
    private function extractImages($body)
    {
        preg_match_all('/<img[^>]+>/i', $body, $images);

        if (!empty($images[0])) {
            foreach ($images[0] as &$img) {
                preg_match_all('/src=("[^"]*")/i', $img, $tmp);
                $img = substr($tmp[1][0], 1, -1);
                unset($tmp);
            }
        }

        return $images;
    }
}
