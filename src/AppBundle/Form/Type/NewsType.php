<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use AppBundle\Form\Type\ContentType;

/**
 * Class NewsType
 * @package AppBundle\Type\Form
 */
class NewsType extends AbstractType
{
    protected $withContent;

    /**
     * @param bool $withContent
     */
    public function __construct($withContent = false)
    {
        $this->withContent = $withContent;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('url', 'text', array(
            'label' => 'Paste URL: ',
            'attr' => array('style' => 'width: 800px; margin: 10px', 'class' => 'form-control'),
            'error_bubbling' => true,
        ));

        if ($this->withContent) {
            $builder->add('content', new ContentType());
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'news';
    }
}