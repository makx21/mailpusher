<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

/**
 * Class RegistrationFormType
 */
class RegistrationFormType extends BaseType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder
            ->add('firstName', 'text', array(
                'attr' => array(
                    'autocomplete' => 'off',
                ),
                'label' => 'register_form.first_name',
            ))
            ->add('lastName', 'text', array(
                'attr' => array(
                    'autocomplete' => 'off',
                ),
                'label' => 'register_form.last_name',
            ))

            ->add('mobileNumber', 'text', array(
                'label' => 'register_form.mobile_number',
            ))
            ->add('country', 'country', array(
                'placeholder' => 'Choose an option',
                'label' => 'register_form.country',
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pusher_registration';
    }
}