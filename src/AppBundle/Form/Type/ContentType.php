<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ContentType
 * @package AppBundle\Type\Form
 */
class ContentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title', 'text', array(
            'attr' => array('style' => 'margin: 10px', 'class' => 'form-control'),
            'label' => 'Title',
        ))
            ->add('body', 'textarea', array(
                'attr' => array('style' => 'margin: 10px', 'class' => 'tinymce'),
                'label' => 'Body ',
            ));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Content',
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'content';
    }
}
