<?php

namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Form\FormBuilderInterface;
use FOS\UserBundle\Form\Type\ProfileFormType as BaseType;

/**
 * Class ProfileFormType
 */
class ProfileFormType extends BaseType
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * Override constructor with inject params
     *
     * @param string        $class
     * @param EntityManager $em
     * @param TokenStorage  $tokenStorage
     */
    public function __construct($class, EntityManager $em, TokenStorage $tokenStorage)
    {
        parent::__construct($class);

        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $user = $this->tokenStorage->getToken()->getUser();

        $builder->add('firstName', 'text', array(
            'label' => 'edit_profile.first_name',
            ))
            ->add('lastName', 'text', array(
                'label' => 'edit_profile.last_name',
            ))
            ->add('mobileNumber', 'text', array('label'=>'edit_profile.mobile_number'))
            ->add('country', 'country', array(
                'data' => $this->em->getRepository('AppBundle:User')->getUserCountry($user->getId()),
                'label' => 'edit_profile.country',
            ))
            ->add('isSubscribed', 'checkbox', array(
                'label' => 'edit_profile.subscribed',
                'required' => false,
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'pusher_user_profile';
    }
}