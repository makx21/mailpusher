<?php

namespace AppBundle\DataFixtures\ORM;

use Hautelook\AliceBundle\Alice\DataFixtureLoader;
use Nelmio\Alice\Fixtures;

/**
 * Class AppFixtures
 * @package AppBundle\DataFixtures\ORM
 */
class AppFixtures extends DataFixtureLoader
{
    protected function getFixtures()
    {
        return  array(
            __DIR__.'/pusher.yml',
        );
    }
}