<?php

namespace AppBundle;

/**
 * Contains all events thrown in our Pusher application
 */
final class PusherEvents
{
    /**
     * The PARSER_ERROR event occurs when parser finished with errors.
     *
     * The event listener receives a AppBundle\EventListeners\ParserErrorListener instance.
     */
    const PARSER_ERROR = 'parser.error';
}
