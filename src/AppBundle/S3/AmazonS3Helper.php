<?php

namespace AppBundle\S3;

use Aws\S3\S3Client;

/**
 * Class AmazonS3Helper
 * @package AppBundle\S3
 */
class AmazonS3Helper
{
    const ACL = 'public-read';

    /**
     * @var string
     */
    protected $key;

    /**
     * @var string
     */
    protected $secret;

    /**
     * @var string
     */
    protected $region;

    /**
     * @var string
     */
    protected $bucket;

    /**
     * @var null | S3Client
     */
    protected $s3 = null;

    /**
     * @param string $key
     * @param string $secret
     * @param string $region
     * @param string $bucket
     */
    public function __construct($key, $secret, $region, $bucket)
    {
        $this->key = $key;
        $this->secret = $secret;
        $this->region = $region;
        $this->bucket = $bucket;
    }

    /**
     * Get instance
     *
     * @return S3Client
     */
    public function getClient()
    {
        if (empty($this->s3)) {
            $this->s3 = S3Client::factory(array(
                'key' => $this->key,
                'secret' => $this->secret,
                'region' => $this->region,
            ));
        }

        return $this->s3;
    }

    /**
     * Upload file
     *
     * @param string $targetPath
     * @param string $fileDir
     *
     * @return mixed
     */
    public function upload($targetPath, $fileDir)
    {
        $res = $this->getClient()->putObject(array(
                'Bucket' => $this->bucket,
                'Key'    => $targetPath,
                'Body'   => fopen($fileDir, 'r'),
                'ACL'    => AmazonS3Helper::ACL,
            ));

        return $res;
    }

    /**
     * Delete folder
     *
     * @param int $id
     */
    public function delete($id)
    {
        $this->getClient()->deleteMatchingObjects($this->bucket, 'images/'.$id.'/');
    }
}
