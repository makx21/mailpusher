<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     * @return Response
     */
    public function unsubscribeAction(Request $request)
    {
        if ($request->get('id') && $request->get('s')) {
            $id = $request->get('id');
            $hash = $request->get('s');

            $expected = md5($id.$this->getParameter('secret'));

            if ($hash === $expected) {
                $em = $this->getDoctrine()->getManager();
                $user = $em->getRepository('AppBundle:User')->find($id);
                $user->setIsSubscribed(0);

                $em->persist($user);
                $em->flush();
            }
        }

        return $this->render('AppBundle:User:unsubscribe.html.twig');
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function preUnsubscribeAction(Request $request)
    {
        $link = '';
        if ($request->get('id') && $request->get('s')) {
            $link = $this->get('router')->generate('app_user_unsubscribe', array(
                'id' => $request->get('id'),
                's' => $request->get('s'),
            ));
        }

        return $this->render('AppBundle:User:preunsubscribe.html.twig', array('link' => $link));
    }
}
