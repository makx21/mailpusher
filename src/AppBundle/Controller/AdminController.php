<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\StreamedResponse;
use AppBundle\Entity\News;
use AppBundle\Form\Type\NewsType;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AdminController
 * @package AppBundle\Controller
 */
class AdminController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $news = $this->getDoctrine()->getRepository('AppBundle:News')->findAll();
        $form = $this->createForm(new NewsType(), new News());
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $news,
            $request->query->get('page', 1)
        );

        return $this->render('AppBundle:Admin:index.html.twig', array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        ));
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function usersAction(Request $request)
    {
        $users = $this->getDoctrine()->getRepository('AppBundle:User')->findAll();
        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $users,
            $request->query->get('page', 1)
        );

        return $this->render('AppBundle:Admin:index_users.html.twig', array(
            'pagination' => $pagination,
        ));
    }

    /**
     * @param Request $request
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function createUpdateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        if (!is_null($id)) {
            $news = $em->getRepository('AppBundle:News')
                ->find($id);

            if (null === $news) {
                throw $this->createNotFoundException('Unable to find News with id '.$id);
            }
        } else {
            $news = new News();
        }

        $form = $this->createForm(new NewsType(!is_null($id)), $news);

        if ($request->getMethod() == 'POST') {
            $form->submit($request);

            if ($form->isValid()) {
                $em->persist($news);
                $em->flush();
            }

            $this->get('session')->getFlashBag()->add('pusher-notice', array(
                'message' => $form->isValid() ? 'News was successfully '.($id ? 'edited' : 'created') : (string) $form->getErrors(),
                'status' => $form->isValid() ? 'success' : 'danger',
            ));

            return $this->redirectToRoute('pusher_admin_homepage');
        }

        return $this->render('AppBundle:Admin:edit.html.twig', array(
            'form' => $form->createView(),
            'id' => $id,
        ));

    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $newsRepo = $this->getDoctrine()->getRepository('AppBundle:News');

        $res = $newsRepo->deleteNews($id);

        if ($res) {
            if (in_array('hasImages', $res)) {
                $this->get('pusher.s3')->delete($id);
            }
            $this->get('session')->getFlashBag()->add('pusher-notice', array(
                'message' => 'News was successfully removed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('pusher_admin_homepage'));
    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveUpAction($id)
    {
        $this->getDoctrine()->getRepository('AppBundle:News')->moveUpNewsPosition($id);

        return $this->redirect($this->generateUrl('pusher_admin_homepage'));
    }

    /**
     * @param integer $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function moveDownAction($id)
    {
        $this->getDoctrine()->getRepository('AppBundle:News')->moveDownNewsPosition($id);

        return $this->redirect($this->generateUrl('pusher_admin_homepage'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function doParseAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('pusher.parser_manager')->run();

            $this->get('session')->getFlashBag()->add('pusher-notice', array(
                'message' => 'News was successfully parsed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('pusher_admin_homepage'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function pushAction()
    {
        if ($this->get('security.authorization_checker')->isGranted('ROLE_SUPER_ADMIN')) {
            $this->get('pusher.pusher')->push();

            $this->get('session')->getFlashBag()->add('pusher-notice', array(
                'message' => 'News was successfully pushed',
                'status' => 'success',
            ));
        }

        return $this->redirect($this->generateUrl('pusher_admin_homepage'));
    }

    /**
     * @param Request $request
     * @return StreamedResponse
     */
    public function getUsersCsvAction(Request $request)
    {
        $addWhere = '';
        $start = $request->query->get('start', null);
        $end = $request->query->get('end', null);

        if ($start === '') {
            $start = null;
        }
        if ($end === '') {
            $end = null;
        }

        if (!is_null($start) && is_null($end)) {
            $addWhere = ' WHERE `created_at` >= '."'".$start."'";
        }
        if (is_null($start) && !is_null($end)) {
            $addWhere = ' WHERE `created_at` <= '."'".$end."'";
        }
        if (!is_null($start) && !is_null($end)) {
            $addWhere = " WHERE `created_at` >= "."'".$start."' AND `created_at` <= "."'$end'";
        }

        $conn = $this->getDoctrine()->getConnection();

        $response = new StreamedResponse();
        $response->setCallback(function() use ($conn, $addWhere) {

            $handle = fopen('php://output', 'w+');

            fputcsv($handle, array('Id', 'Username', 'Email', 'Country', 'Subscribed', 'Created'), ';');

            $results = $conn->query("SELECT id, username, email, country, is_subscribed, created_at FROM fos_user ".$addWhere);

            while( $row = $results->fetch() ) {
                fputcsv($handle, array(
                    $row['id'],
                    $row['username'],
                    $row['email'],
                    $row['country'],
                    $row['is_subscribed'],
                    $row['created_at'],
                ), ';');
            }

            fclose($handle);
        });

        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment; filename="pusher_users.csv"');

        return $response;
    }
}
