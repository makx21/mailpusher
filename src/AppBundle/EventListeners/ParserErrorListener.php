<?php

namespace AppBundle\EventListeners;

use AppBundle\Events\ParserErrorEvent;
use AppBundle\PusherEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ParserErrorListener
 * @package AppBundle\EventListeners
 */
class ParserErrorListener implements EventSubscriberInterface
{
    /**
     * @var $logger
     */
    protected $logger;

    /**
     * @param $logger
     */
    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            PusherEvents::PARSER_ERROR => array(
                array('onError', 10),
            ),
        );
    }

    /**
     * @param ParserErrorEvent $event
     */
    public function onError(ParserErrorEvent $event)
    {
        $this->logger->error('Parser Error: '.$event->getMessage());
    }
}