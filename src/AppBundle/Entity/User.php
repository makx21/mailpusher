<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    use ORMBehaviors\Timestampable\Timestampable;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $firstName
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your First name")
     * @Assert\Length(
     *     min=1,
     *     max="255",
     *     minMessage="The First Name is too short",
     *     maxMessage="The First Name is too long")
     */
    protected $firstName;

    /**
     * @var string $lastName
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your Last name")
     * @Assert\Length(
     *     min=1,
     *     max="255",
     *     minMessage="The Last Name is too short. It must be at least 2 characters long",
     *     maxMessage="The Last Name is too long")
     */
    protected $lastName;

    /**
     * @var string $mobileNumber
     *
     * @ORM\Column(name="mobile_number", type="string", nullable=true)
     *
     * @Assert\NotBlank(message="Please enter your mobile number")
     */
    protected $mobileNumber;

    /**
     * @var string $country
     * @ORM\Column(name="country", type="string")
     */
    protected $country;

    /**
     * @ORM\Column(type="boolean", name="is_subscribed", options={"default": true})
     */
    protected $isSubscribed;

    /**
     * @ORM\Column(type="integer", name="mail_count", options={"default": 0})
     */
    protected $mailCount;

    /**
     * Construct
     */
    public function __construct()
    {
        parent::__construct();
        $this->setIsSubscribed(1);
        $this->setMailCount(0);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set mobileNumber
     *
     * @param string $mobileNumber
     * @return User
     */
    public function setMobileNumber($mobileNumber)
    {
        $this->mobileNumber = $mobileNumber;

        return $this;
    }

    /**
     * Get mobileNumber
     *
     * @return string
     */
    public function getMobileNumber()
    {
        return $this->mobileNumber;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set isSubscribed
     *
     * @param bool $isSubscribed
     * @return User
     */
    public function setIsSubscribed($isSubscribed)
    {
        $this->isSubscribed = $isSubscribed;

        return $this;
    }

    /**
     * Get isSubscribed
     *
     * @return bool
     */
    public function getIsSubscribed()
    {
        return $this->isSubscribed;
    }

    /**
     * Set mailCount
     *
     * @param integer $mailCount
     * @return User
     */
    public function setMailCount($mailCount)
    {
        $this->mailCount = $mailCount;

        return $this;
    }

    /**
     * Get mailCount
     *
     * @return integer
     */
    public function getMailCount()
    {
        return $this->mailCount;
    }
}
