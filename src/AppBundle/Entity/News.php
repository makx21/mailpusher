<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @UniqueEntity("url")
 * @ORM\Entity(repositoryClass="AppBundle\Entity\Repository\NewsRepository")
 * @ORM\Table(name="news")
 */
class News
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="url", type="string", length=255, unique=true)
     * @Assert\NotBlank()
     * @Assert\Url()
     */
    protected $url;

    /**
     * @ORM\Column(type="boolean", name="parsed", options={"default": false})
     */
    protected $parsed = 0;

    /**
     * @ORM\Column(type="boolean", name="sending", options={"default": false})
     */
    protected $sending = 0;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer")
     */
    protected $position;

    /**
     * @ORM\OneToOne(targetEntity="Content")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     **/
    protected $content;

    /**
     * @ORM\OneToMany(targetEntity="Images", mappedBy="news")
     **/
    protected $images;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return News
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set parsed
     *
     * @param boolean $parsed
     * @return News
     */
    public function setParsed($parsed)
    {
        $this->parsed = $parsed;

        return $this;
    }

    /**
     * Get parsed
     *
     * @return boolean
     */
    public function getParsed()
    {
        return $this->parsed;
    }

    /**
     * Set sending
     *
     * @param boolean $sending
     * @return News
     */
    public function setSending($sending)
    {
        $this->sending = $sending;

        return $this;
    }

    /**
     * Get sending
     *
     * @return boolean
     */
    public function getSending()
    {
        return $this->sending;
    }

    /**
     * Set position
     *
     * @param integer $position
     * @return News
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set content
     *
     * @param \AppBundle\Entity\Content $content
     * @return News
     */
    public function setContent(\AppBundle\Entity\Content $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return \AppBundle\Entity\Content
     */
    public function getContent()
    {
        return $this->content;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add images
     *
     * @param \AppBundle\Entity\Images $images
     * @return News
     */
    public function addImage(\AppBundle\Entity\Images $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AppBundle\Entity\Images $images
     */
    public function removeImage(\AppBundle\Entity\Images $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }
}
